package com.jiaxw.util.encrypt;

import java.security.MessageDigest;

/**
 * MD5加密工具类
 *
 * @author jiaxw
 * @date 2020/10/27 16:57
 */
public class Md5Util {

    /**
     * 32位小写
     *
     * @param param 字符串
     * @return
     */
    public static String lowerCase32Md5(String param) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(param.getBytes());
            byte[] b = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0) {
                    i += 256;
                }
                if (i < 16) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(i));
            }
            return buf.toString();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 32位大写
     *
     * @param param 字符串
     * @return
     */
    public static String upperCase32Md5(String param) {
        return lowerCase32Md5(param).toUpperCase();
    }

    /**
     * 16位小写
     *
     * @param param 字符串
     * @return
     */
    public static String lowerCase16Md5(String param) {
        return lowerCase32Md5(param).substring(8, 24);
    }

    /**
     * 16位大写
     *
     * @param param 字符串
     * @return
     */
    public static String upperCase16Md5(String param) {
        return lowerCase32Md5(param).substring(8, 24).toUpperCase();
    }

    /**
     * 32位小写+salt
     *
     * @param param 需要加密的字符串
     * @param salt  盐值
     * @return
     */
    public static String saltLowerCase32Md5(String param, String salt) {
        return lowerCase32Md5(param + salt);
    }

    /**
     * 32位大写+salt
     *
     * @param param 需要加密的字符串
     * @param salt  盐值
     * @return
     */
    public static String saltUpperCase32Md5(String param, String salt) {
        return upperCase32Md5(param + salt);
    }

    /**
     * 16位小写+salt
     *
     * @param param 需要加密的字符串
     * @param salt  盐值
     * @return
     */
    public static String saltLowerCase16Md5(String param, String salt) {
        return lowerCase16Md5(param + salt);
    }

    /**
     * 16位大写+salt
     *
     * @param param 需要加密的字符串
     * @param salt  盐值
     * @return
     */
    public static String saltUpperCase16Md5(String param, String salt) {
        return upperCase16Md5(param + salt);
    }

}

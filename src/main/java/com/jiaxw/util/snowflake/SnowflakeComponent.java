package com.jiaxw.util.snowflake;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 分布式下使用雪花算法
 *
 * @author jiaxw
 * @date 2020/12/30 14:17
 */
@Slf4j
@Component
public class SnowflakeComponent {

    @Value("${server.workId}")
    private long workId;

    @Value("${server.datacenterId}")
    private long datacenterId;

    private static volatile SnowflakeIdWorker instance;

    public SnowflakeIdWorker getInstance() {
        if (instance == null) {
            synchronized (SnowflakeIdWorker.class) {
                if (instance == null) {
                    log.info("when instance, workId = {}, datacenterId = {}", workId, datacenterId);
                    instance = new SnowflakeIdWorker(workId, datacenterId);
                }
            }
        }
        return instance;
    }

}

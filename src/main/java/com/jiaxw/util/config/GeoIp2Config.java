package com.jiaxw.util.config;

import com.maxmind.geoip2.DatabaseReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.IOException;

/**
 * GeoIp2配置类
 *
 * @author jiaxw
 * @date 2020/10/30 17:30
 */
@Slf4j
//@Configuration
public class GeoIp2Config {

    @Bean(destroyMethod = "close")
    public DatabaseReader databaseReader() {
        try {
            String path = GeoIp2Config.class.getResource("/GeoLite2-City.mmdb").getPath();
            return new DatabaseReader.Builder(new File(path)).build();
        } catch (IOException e) {
            log.error("IPDatabase error:", e.getMessage());
            return null;
        }
    }

}

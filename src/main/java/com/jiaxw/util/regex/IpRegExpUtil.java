package com.jiaxw.util.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * IP校验工具类
 *
 * @author jiaxw
 * @date 2020/10/28 18:10
 */
public class IpRegExpUtil {

    /**
     * ipv4正则表达式
     */
    private static final String IPV4_REGEXP = "(([0,1]?\\d?\\d|2[0-4]\\d|25[0-5])\\.){3}([0,1]?\\d?\\d|2[0-4]\\d|25[0-5])";

    /**
     * IPV4 正则校验
     *
     * @param ip IPV4
     * @return
     */
    public static boolean isIpV4(String ip) {
        if (null == ip || "".equals(ip)) {
            return false;
        }
        Pattern pattern = Pattern.compile(IPV4_REGEXP);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }

}

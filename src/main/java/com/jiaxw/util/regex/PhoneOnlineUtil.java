package com.jiaxw.util.regex;

import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * 在线查询电话号码有效性工具类
 *
 * @author jiaxw
 * @date 2020/10/28  14:30
 */
public class PhoneOnlineUtil {

    /**
     * 淘宝
     */
    public static final String PHONE_SERVICE_TAO_BAO = "http://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel=";

    /**
     * 360
     */
    public static final String PHONE_SERVICE_360 = "https://cx.shouji.360.cn/phonearea.php?number=";

    /**
     * 校验手机号
     *
     * @param service 服务商
     * @param phone   手机号
     * @return
     */
    public static Map<String, String> check(String service, String phone) {
        StringBuilder sb = new StringBuilder();

        // 访问服务商提供的在线查询接口
        getUrlService(service, phone, sb);

        // 淘宝返回结果处理
        if (PHONE_SERVICE_TAO_BAO.equals(service)) {
            return getTaoBaoPhoneMap(sb);
        }

        // 360返回结果处理
        if (PHONE_SERVICE_360.equals(service)) {
            return get360PhoneMap(sb);
        }

        return null;
    }

    /**
     * 访问服务商提供的在线查询接口
     *
     * @param service 服务商
     * @param phone   手机号
     * @param sb      返回的信息
     */
    private static void getUrlService(String service, String phone, StringBuilder sb) {
        try {
            URL url = new URL(service + phone);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            PrintWriter out = null;
            conn.setRequestProperty("accept", "*/*");
            conn.setDoOutput(true);
            out = new PrintWriter(conn.getOutputStream());
            out.flush();
            InputStream is = conn.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "gb2312"));
            String result = "";
            while ((result = br.readLine()) != null) {
                sb.append(result);
            }
            is.close();
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 解析淘宝返回的手机信息
     *
     * @param sb
     * @return
     */
    private static Map<String, String> getTaoBaoPhoneMap(StringBuilder sb) {
        String str = sb.toString().replaceAll("__GetZoneResult_ = ", "");
        JSONObject data = JSONObject.parseObject(str);

        Map<String, String> map = new HashMap<>(8);

        if (data.size() < 7) {
            map.put("effective", "false");
            return map;
        }
        String carrier = data.get("carrier").toString();
        String province = data.get("province").toString();
        String areaVid = data.get("areaVid").toString();
        String catName = data.get("catName").toString();
        String telString = data.get("telString").toString();
        String mts = data.get("mts").toString();
        String ispVid = data.get("ispVid").toString();


        map.put("effective", "true");
        map.put("carrier", carrier);
        map.put("province", province);
        map.put("areaVid", areaVid);
        map.put("catName", catName);
        map.put("telString", telString);
        map.put("mts", mts);
        map.put("ispVid", ispVid);
        return map;
    }

    /**
     * 解析360返回的手机信息
     *
     * @param sb
     * @return
     */
    private static Map<String, String> get360PhoneMap(StringBuilder sb) {
        JSONObject resultObj = JSONObject.parseObject(sb.toString());
        JSONObject data = (JSONObject) resultObj.get("data");

        Map<String, String> map = new HashMap<>(4);

        if (null == data || data.size() < 3) {
            map.put("effective", "false");
            return map;
        }

        String province = data.get("province").toString();
        String city = data.get("city").toString();
        String sp = data.get("sp").toString();

        map.put("effective", "true");
        map.put("province", province);
        map.put("city", city);
        map.put("sp", sp);
        return map;
    }

}

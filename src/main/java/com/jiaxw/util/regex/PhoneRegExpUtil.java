package com.jiaxw.util.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式 电话号码有效性工具类
 *
 * @author jiaxw
 * @date 2020/10/28  16:30
 */
public class PhoneRegExpUtil {

    /**
     * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
     * 此方法中前三位格式有：
     * 13+任意数
     * 14+(5,7)
     * 15+除4的任意数
     * 17+(0,6,7,8)
     * 18+任意数
     */
    private static final String CHINA_PHONE_REGEXP = "^((13[0-9])|(14[5,7])|(15[^4])|(17[0,6-8])|(18[0-9]))\\d{8}$";

    /**
     * 香港手机号码8位数，5|6|8|9开头+7位任意数
     */
    private static final String HK_PHONE_REGEXP = "^(5|6|8|9)\\d{7}$";

    /**
     * 大陆号码或香港号码均可
     *
     * @param phone 手机号
     * @return
     * @throws
     */
    public static boolean isPhoneLegal(String phone) {
        return isChinaPhoneLegal(phone) || isHkPhoneLegal(phone);
    }

    /**
     * 中国大陆手机号校验
     *
     * @param phone 手机号
     * @return
     */
    public static boolean isChinaPhoneLegal(String phone) {
        return regular(phone, CHINA_PHONE_REGEXP);
    }

    /**
     * 香港手机号码8位数，5|6|8|9开头+7位任意数
     *
     * @param phone 手机号
     * @return
     */
    public static boolean isHkPhoneLegal(String phone) {
        return regular(phone, HK_PHONE_REGEXP);
    }

    /**
     * 正则校验
     *
     * @param str    待校验字符串
     * @param regExp 正则表达式
     * @return
     */
    private static boolean regular(String str, String regExp) {
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

}

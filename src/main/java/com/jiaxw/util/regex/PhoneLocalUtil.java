package com.jiaxw.util.regex;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberToCarrierMapper;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.google.i18n.phonenumbers.geocoding.PhoneNumberOfflineGeocoder;
import org.springframework.util.StringUtils;

import java.util.Locale;

/**
 * 本地查询电话号码有效性工具类
 *
 * @author jiaxw
 * @date 2020/10/28  16:30
 */
public class PhoneLocalUtil {

    /**
     * 手机号码工具类
     */
    private static PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    /**
     * 手机号码运营商工具类
     */
    private static PhoneNumberToCarrierMapper carrierMapper = PhoneNumberToCarrierMapper.getInstance();

    /**
     * 手机号码位置工具类
     */
    private static PhoneNumberOfflineGeocoder geocoder = PhoneNumberOfflineGeocoder.getInstance();

    /**
     * 根据国家代码和手机号  判断手机号是否有效
     *
     * @param phoneNumber 手机号
     * @param countryCode 国家代码
     * @return
     */
    public static boolean checkPhoneNumber(String phoneNumber, String countryCode) {
        int code = Integer.valueOf(countryCode);
        long phone = Long.valueOf(phoneNumber);
        PhoneNumber pn = new PhoneNumber();
        pn.setCountryCode(code);
        pn.setNationalNumber(phone);
        return phoneNumberUtil.isValidNumber(pn);
    }

    /**
     * 根据国家代码和手机号  判断手机运营商
     *
     * @param phoneNumber 手机号
     * @param countryCode 国家代码
     * @return
     */
    public static String getCarrier(String phoneNumber, String countryCode) {
        int code = Integer.valueOf(countryCode);
        long phone = Long.valueOf(phoneNumber);
        PhoneNumber pn = new PhoneNumber();
        pn.setCountryCode(code);
        pn.setNationalNumber(phone);

        // 返回结果只有英文，自己转成成中文
        String carrierEn = carrierMapper.getNameForNumber(pn, Locale.ENGLISH);
        String carrierZh = "";
        carrierZh += geocoder.getDescriptionForNumber(pn, Locale.CHINESE);
        switch (carrierEn) {
            case "China Mobile":
                carrierZh += "移动";
                break;
            case "China Unicom":
                carrierZh += "联通";
                break;
            case "China Telecom":
                carrierZh += "电信";
                break;
            default:
                break;
        }
        return carrierZh;
    }

    /**
     * 根据国家代码和手机号  手机归属地
     *
     * @param phoneNumber 手机号
     * @param countryCode 国家代码
     * @return
     */
    public static String getGeo(String phoneNumber, String countryCode) {
        int code = Integer.valueOf(countryCode);
        long phone = Long.valueOf(phoneNumber);
        PhoneNumber pn = new PhoneNumber();
        pn.setCountryCode(code);
        pn.setNationalNumber(phone);
        return geocoder.getDescriptionForNumber(pn, Locale.CHINESE);
    }

    /**
     * 根据国家代码和手机号 得到手机的归宿地编码
     *
     * @param phoneNumber 手机号
     * @param countryCode 国家代码
     * @return
     */
    public static String getPhoneRegionCode(String phoneNumber, String countryCode) {
        String areaName = getGeo(phoneNumber, countryCode);
        if (StringUtils.isEmpty(areaName)) {
            return "";
        }
        if (areaName.length() < 3) {
            return "";
        }
        return areaName;
    }

    /**
     * 查询手机归属地
     *
     * @param tel 手机号
     * @return
     */
    public static String checkPhone(String tel) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        PhoneNumberOfflineGeocoder phoneNumberOfflineGeocoder = PhoneNumberOfflineGeocoder.getInstance();
        String language = "CN";
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            phoneNumber = phoneUtil.parse(tel, language);
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
        // 手机号码归属城市 city
        return phoneNumberOfflineGeocoder.getDescriptionForNumber(phoneNumber, Locale.CHINA);
    }

}


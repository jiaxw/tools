package com.jiaxw.util.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 邮箱校验工具类
 *
 * @author jiaxw
 * @date 2020/10/28 17:40
 */
public class EmailUtil {

    private static final String EMAIL_REGEXP = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

    /**
     * 邮箱校验
     *
     * @param email 邮箱
     * @return
     */
    public static boolean isEmail(String email) {
        if (null == email || "".equals(email)) {
            return false;
        }
        Pattern pattern = Pattern.compile(EMAIL_REGEXP);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}

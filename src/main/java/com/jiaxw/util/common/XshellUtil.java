package com.jiaxw.util.common;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Xshell远程连接工具类
 *
 * @author jiaxw
 * @date 2020/10/27 16:26
 * @url http://www.jcraft.com/jsch/
 */
public class XshellUtil {

    /**
     * 远程执行命令并返回结果，调用过程是同步的（执行完才会返回）
     *
     * @param host     主机名
     * @param user     用户名
     * @param password 密码（idRsa = true 时，password为用户密码；idRsa = false 时，password为本地私钥地址）
     * @param port     端口
     * @param command  命令
     * @param idRsa    本地私钥地址（需要提前把公钥放在待连接服务器）
     * @return
     */
    public static String exec(String host, String user, String password, int port, String command, boolean idRsa) {
        String result = "";
        Session session = null;
        ChannelExec openChannel = null;
        try {
            JSch jsch = new JSch();
            if (idRsa) {
                jsch.addIdentity(password);
            }
            session = jsch.getSession(user, host, port);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            if (!idRsa) {
                session.setPassword(password);
            }
            session.connect();
            openChannel = (ChannelExec) session.openChannel("exec");
            openChannel.setCommand(command);
            int exitStatus = openChannel.getExitStatus();
            openChannel.connect();
            InputStream in = openChannel.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String buf = null;
            while (null != (buf = reader.readLine())) {
                result += new String(buf.getBytes("gbk"), "UTF-8") + "\r\n";
            }
        } catch (JSchException | IOException e) {
            result += e.getMessage();
        } finally {
            if (null != openChannel && !openChannel.isClosed()) {
                openChannel.disconnect();
            }
            if (null != session && session.isConnected()) {
                session.disconnect();
            }
        }
        return result;
    }

}

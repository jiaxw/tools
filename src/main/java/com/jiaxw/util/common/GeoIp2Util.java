package com.jiaxw.util.common;

import com.maxmind.geoip2.DatabaseReader;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;

/**
 * IP定位
 *
 * @author jiaxw
 * @date 2020/10/30 17:30
 */
public class GeoIp2Util {

    /**
     * 获取IP地址
     *
     * @param request
     * @return
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        if (ip == null) {
            ip = " ";
        }
        return ip;
    }

    /**
     * @param reader
     * @param ip
     * @return
     * @throws Exception
     * @description: 获得国家
     */
    public static String getCountry(DatabaseReader reader, String ip) throws Exception {
        return reader.city(InetAddress.getByName(ip)).getCountry().getNames().get("zh-CN");
    }

    /**
     * @param reader
     * @param ip
     * @return
     * @throws Exception
     * @description: 获得省份
     */
    public static String getProvince(DatabaseReader reader, String ip) throws Exception {
        return reader.city(InetAddress.getByName(ip)).getMostSpecificSubdivision().getNames().get("zh-CN");
    }

    /**
     * @param reader
     * @param ip
     * @return
     * @throws Exception
     * @description: 获得城市
     */
    public static String getCity(DatabaseReader reader, String ip) throws Exception {
        return reader.city(InetAddress.getByName(ip)).getCity().getNames().get("zh-CN");
    }

    /**
     * 获得详细地址
     *
     * @param reader
     * @param ip
     * @return
     * @throws Exception
     */
    public static String getAddress(DatabaseReader reader, String ip) throws Exception {
        String country = reader.city(InetAddress.getByName(ip)).getCountry().getNames().get("zh-CN");
        String province = reader.city(InetAddress.getByName(ip)).getMostSpecificSubdivision().getNames().get("zh-CN");
        String city = reader.city(InetAddress.getByName(ip)).getCity().getNames().get("zh-CN");
        String f = "-";
        return country + f + province + f + city;
    }

    /**
     * @param reader
     * @param ip
     * @return
     * @throws Exception
     * @description: 获得经度
     */
    public static Double getLongitude(DatabaseReader reader, String ip) throws Exception {
        return reader.city(InetAddress.getByName(ip)).getLocation().getLongitude();
    }

    /**
     * @param reader
     * @param ip
     * @return
     * @throws Exception
     * @description: 获得纬度
     */
    public static Double getLatitude(DatabaseReader reader, String ip) throws Exception {
        return reader.city(InetAddress.getByName(ip)).getLocation().getLatitude();
    }

}

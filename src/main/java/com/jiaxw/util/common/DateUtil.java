package com.jiaxw.util.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author jiaxw
 * @date 2020/10/28  10:30
 */
public class DateUtil {

    /**
     * 仅显示年月日
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 显示年月日时分秒
     */
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 仅显示时分秒
     */
    public static final String TIME_FORMAT = "HH:mm:ss";

    /**
     * 显示时间戳 年月日时分秒
     */
    public static final String TIME_STAMP_FORMAT = "yyyyMMddHHmmss";

    /**
     * 获取时间戳
     *
     * @return
     */
    public static String getTimeStamp() {
        long currentTimeMillis = System.currentTimeMillis();
        Date date = new Date(currentTimeMillis);
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_STAMP_FORMAT);
        return sdf.format(date);
    }

    /**
     * String转日期
     *
     * @param str    String
     * @param format 格式化
     * @return
     */
    public static Date getFormatDate(String str, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = sdf.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 日期转String
     *
     * @param date   日期
     * @param format 格式化
     * @return
     */
    public static String getFormatString(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String str = sdf.format(date);
        return str;
    }

    /**
     * 获得当前日期 yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static String getCurrentDateTime() {
        return getCurrentDateTime(DATETIME_FORMAT);
    }

    /**
     * 获得当前日期 yyyy-MM-dd
     *
     * @return
     */
    public static String getCurrentDate() {
        return getCurrentDateTime(DATE_FORMAT);
    }

    /**
     * 获得当前日期 HH:mm:ss
     *
     * @return
     */
    public static String getCurrentTime() {
        return getCurrentDateTime(TIME_FORMAT);
    }

    /**
     * 获取当前时间
     *
     * @param format 格式化显示
     * @return
     */
    private static String getCurrentDateTime(String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        Date date = new Date();
        return df.format(date);
    }

    /**
     * 计算两个时间之间相差的天数(Date类型)
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static Integer getDaysBetween(Date start, Date end) {
        int days = (int) ((end.getTime() - start.getTime()) / (1000 * 3600 * 24));
        return days;
    }

    /**
     * 计算两个时间之间相差的天数(String类型)
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static Integer getDaysBetween(String start, String end) {
        SimpleDateFormat df = new SimpleDateFormat(DATETIME_FORMAT);
        try {
            Date date1 = df.parse(start);
            Date date2 = df.parse(end);
            int days = (int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
            return days;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 指定日期，修改天数
     *
     * @param date
     * @param num
     * @return
     */
    public static Date addDay(Date date, int num) {
        Calendar start = Calendar.getInstance();
        start.setTime(date);
        start.add(Calendar.DATE, num);
        return start.getTime();
    }

    /**
     * 指定日期，修改天数小时数
     *
     * @param date
     * @param num
     * @param hour
     * @return
     */
    public static Date addHour(Date date, int num, int hour) {
        Calendar start = Calendar.getInstance();
        start.setTime(date);
        start.add(Calendar.DATE, num);
        start.add(Calendar.HOUR, hour);
        return start.getTime();
    }

    /**
     * 获取当前日期是星期几
     *
     * @param date
     * @return 当前日期是星期几
     */
    public static Integer getWeekOfDate(Date date) {
        Integer[] weekDays = {7, 1, 2, 3, 4, 5, 6};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) {
            w = 0;
        }
        return weekDays[w];
    }

    /**
     * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
     *
     * @param nowTime     当前时间
     * @param dateSection 时间区间
     * @param format      格式化样式
     * @return
     */
    public static boolean isEffectiveDate(Date nowTime, String dateSection, String format) {
        try {
            String[] times = dateSection.split(",");
            Date startTime = new SimpleDateFormat(format).parse(times[0]);
            Date endTime = new SimpleDateFormat(format).parse(times[1]);
            if (nowTime.getTime() == startTime.getTime() || nowTime.getTime() == endTime.getTime()) {
                return true;
            }

            Calendar date = Calendar.getInstance();
            date.setTime(nowTime);

            Calendar begin = Calendar.getInstance();
            begin.setTime(startTime);

            Calendar end = Calendar.getInstance();
            end.setTime(endTime);

            if (isSameDay(date, begin) || isSameDay(date, end)) {
                return true;
            }
            if (date.after(begin) && date.before(end)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (null != cal1 && null != cal2) {
            return cal1.get(0) == cal2.get(0) && cal1.get(1) == cal2.get(1) && cal1.get(6) == cal2.get(6);
        } else {
            throw new IllegalArgumentException("The date must not be null");
        }
    }

    /**
     * 获取当前日期前一天
     *
     * @param date   时间
     * @param format 格式化
     * @return
     */
    public static String getBeforeDay(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();
        return sdf.format(date);
    }

    /**
     * 获取最近一周的时间
     *
     * @param date   时间
     * @param format 格式化
     * @return
     */
    public static String getOneWeeks(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -7);
        Date monday = c.getTime();
        String preMonday = sdf.format(monday);
        return preMonday;
    }

    /**
     * 获取最近一个月
     *
     * @param date   时间
     * @param format 格式化
     * @return
     */
    public static String getOneMonth(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, -1);
        Date monday = c.getTime();
        String preMonday = sdf.format(monday);
        return preMonday;
    }

    /**
     * 获取最近三个月
     *
     * @param date   时间
     * @param format 格式化
     * @return
     */
    public static String getThreeMonth(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, -3);
        Date monday = c.getTime();
        String preMonday = sdf.format(monday);
        return preMonday;
    }

    /**
     * 获取最近一年
     *
     * @param date   时间
     * @param format 格式化
     * @return
     */
    public static String getOneYear(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, -1);
        Date start = c.getTime();
        String startDay = sdf.format(start);
        return startDay;
    }

}

package com.jiaxw.util.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * 常用工具类
 *
 * @author jiaxw
 * @date 2020/10/29  15:10
 */
public class CommonUtil {

    /**
     * 显示时间戳 年月日时分秒
     */
    public static final String TIME_STAMP_FORMAT = "yyyyMMddHHmmss";

    /**
     * 纯数字
     */
    public static final String TYPE_DIGITAL = "digital";

    /**
     * 纯小写字母
     */
    public static final String TYPE_SMALL = "small";

    /**
     * 纯大写字母
     */
    public static final String TYPE_BIG = "big";

    /**
     * 大小写字母+数字
     */
    public static final String TYPE_MIXTURE = "mixture";

    /**
     * 获取32位UUID
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 获取时间戳
     *
     * @return
     */
    public static String getTimeStamp() {
        long currentTimeMillis = System.currentTimeMillis();
        Date date = new Date(currentTimeMillis);
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_STAMP_FORMAT);
        return sdf.format(date);
    }

    /**
     * 获取三位随机数
     *
     * @return
     */
    public static String getRandom() {
        int number = (int) (Math.random() * 900 + 100);
        return Integer.toString(number);
    }

    /**
     * 获取时间戳+三位随机数
     *
     * @return
     */
    public static String getTimeStampAndRandom() {
        return getTimeStamp() + getRandom();
    }

    /**
     * 获取随机验证码
     *
     * @param length 长度
     * @return
     */
    public static String getCaptcha(int length) {
        String val = "";
        Random random = new Random();
        // 参数length，表示生成几位随机数
        for (int i = 0; i < length; i++) {
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            // 输出字母还是数字
            if ("char".equalsIgnoreCase(charOrNum)) {
                // 输出是大写字母还是小写字母
                int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val += (char) (random.nextInt(26) + temp);
            } else if ("num".equalsIgnoreCase(charOrNum)) {
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }

    /**
     * 获取随机验证码 可配置模式+无重复
     *
     * @param type   随机验证码形式
     * @param length 长度
     * @return
     */
    public static String getCaptcha(String type, int length) {
        // 随机验证码
        String randomCode = "";
        // 随机形式
        String model = "";
        // 随机形式长度
        int count = 0;
        if (CommonUtil.TYPE_DIGITAL.equals(type)) {
            model = "0123456789";
            count = 10;
        }
        if (CommonUtil.TYPE_SMALL.equals(type)) {
            model = "abcdefghijklmnopqrstuvwxyz";
            count = 26;
        }
        if (CommonUtil.TYPE_BIG.equals(type)) {
            model = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            count = 26;
        }
        if (CommonUtil.TYPE_MIXTURE.equals(type)) {
            model = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            count = 62;
        }

        // 生成随机码
        char[] m = model.toCharArray();
        for (int j = 0; j < length; j++) {
            char c = m[(int) (Math.random() * count)];
            // 保证随机数之间没有重复的
            if (randomCode.contains(String.valueOf(c))) {
                j--;
                continue;
            }
            randomCode += c;
        }
        return randomCode;
    }

}

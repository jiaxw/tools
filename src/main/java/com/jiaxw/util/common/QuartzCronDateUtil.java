package com.jiaxw.util.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期转化成 cron 工具类
 *
 * @author jiaxw
 * @date 2020/12/1 10:47
 */
public class QuartzCronDateUtil {

    /**
     * 功能描述：日期转换cron表达式时间格式
     *
     * @param date
     * @param dateFormat
     * @return
     */
    public static String formatDateByPattern(Date date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String formatTimeStr = null;
        if (null != date) {
            formatTimeStr = sdf.format(date);
        }
        return formatTimeStr;
    }

    /**
     * 获取 Cron 表达式
     *
     * @param date
     * @return
     */
    public static String getCron(Date date) {
        String dateFormat = "ss mm HH dd MM ? yyyy";
        return formatDateByPattern(date, dateFormat);
    }

}
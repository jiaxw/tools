package com.jiaxw.util.common;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * JDK 1.8 时间日期工具类（线程安全）
 * LocalDate、LocalTime、LocalDateTime类;
 * TemporalAdjuster接口 —— 时间校正器;
 * Instant 时间戳类;
 * Duration类 —— 用于计算两个“时间”间隔的类;
 * Period类 —— 用于计算两个“日期”间隔的类;
 * DateTimeFormatter类 —— 解析和格式化日期或时间的类;
 * ZonedDate、ZonedTime、ZonedDateTime —— 带时区的时间或日期;
 * ZoneID —— 世界时区类。
 *
 * @author jiaxw
 * @date 2020/11/11 16:54
 */
public class LocalDateUtil {

    /**
     * 仅显示年月日
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 显示年月日时分秒
     */
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 仅显示时分秒
     */
    public static final String TIME_FORMAT = "HH:mm:ss";

    /**
     * 显示时间戳 年月日时分秒
     */
    public static final String TIME_STAMP_FORMAT = "yyyyMMddHHmmss";

    /**
     * 获取13位时间戳
     *
     * @return
     */
    public static Long getTimeStampMilli() {
        return Instant.now().toEpochMilli();
    }

    /**
     * 将Long类型的时间戳转换成String
     *
     * @param time   Long类型的13位时间戳(10位会有问题)
     * @param format 例:yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String convertTimeToString(Long time, String format) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
            return dtf.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将字符串转日期成Long类型的13位时间戳
     *
     * @param time 例:yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Long convertTimeToLong(String time) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATETIME_FORMAT);
            LocalDateTime parse = LocalDateTime.parse(time, dtf);
            return LocalDateTime.from(parse).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取当前时间，自定义格式
     *
     * @param format 例:yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getLocalDateTime(String format) {
        try {
            LocalDateTime ldt = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return ldt.format(formatter);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}

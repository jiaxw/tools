package com.jiaxw.util.common;

import lombok.extern.slf4j.Slf4j;

import java.io.*;

/**
 * Serializable 工具类
 *
 * @author jiaxw
 * @date 2020/12/1 10:47
 */
@Slf4j
public class SerializableUtil {

    /**
     * 序列化
     *
     * @param object
     * @return byte[]
     * @author gourd.hu
     * @date 2018/9/4 15:14
     */
    public static byte[] serializable(Object object) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(object);
            byte[] bytes = baos.toByteArray();
            return bytes;
        } catch (IOException e) {
            log.error("SerializableUtil工具类序列化出现IOException异常:{}", e.getStackTrace());
        }
        return null;
    }

    /**
     * 反序列化
     *
     * @param bytes
     * @return java.lang.Object
     * @author gourd.hu
     * @date 2018/9/4 15:14
     */
    public static Object unserializable(byte[] bytes) {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             ObjectInputStream ois = new ObjectInputStream(bais)) {
            return ois.readObject();
        } catch (ClassNotFoundException e) {
            log.error("SerializableUtil工具类反序列化出现ClassNotFoundException异常:{}", e.getStackTrace());
        } catch (IOException e) {
            log.error("SerializableUtil工具类反序列化出现IOException异常:{}", e.getStackTrace());
        }
        return null;
    }

}
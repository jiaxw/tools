package com.jiaxw.util.controller;

import com.wf.captcha.ArithmeticCaptcha;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * 简单验证码测试
 *
 * @author jiaxw
 * @date 2020/10/30 10:51
 */
@Slf4j
@RestController
public class EasyCaptchaController {

    /**
     * 生成验证码
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/getCode")
    public void getCode(HttpServletResponse response) throws Exception {
        ServletOutputStream outputStream = response.getOutputStream();

        // 算术验证码 数字加减乘
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(120, 40);

        // 中文验证码
        // ChineseCaptcha captcha = new ChineseCaptcha(120, 40);

        // 英文与数字验证码
        // SpecCaptcha captcha = new SpecCaptcha(120, 40);

        // 英文与数字动态验证码
        // GifCaptcha captcha = new GifCaptcha(120, 40);

        // 中文动态验证码
        // ChineseGifCaptcha captcha = new ChineseGifCaptcha(120, 40);

        // 几位数运算，默认是两位
        captcha.setLen(2);

        // 获取运算的结果
        String result = captcha.text();
        log.info("验证码:{}", result);
        captcha.out(outputStream);
    }

}

package com.jiaxw.util.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 压缩包工具类
 *
 * @author jiaxw
 * @date 2020/10/29 15:52
 */
@Slf4j
public class ZipUtil {

    /**
     * 文件压缩 String 参数
     *
     * @param srcFile 源文件路径
     * @param zipFile 压缩文件路径
     * @throws IOException
     */
    public static boolean doCompress(String srcFile, String zipFile) {
        return doCompress(new File(srcFile), new File(zipFile));
    }

    /**
     * 文件压缩 File 参数
     *
     * @param srcFile 目录或者单个文件
     * @param zipFile 压缩后的ZIP文件
     */
    public static boolean doCompress(File srcFile, File zipFile) {
        // 待压缩文件路径不存在
        if (!srcFile.exists()) {
            log.info("文件下载路径不存在!");
            return false;
        }
        // 压缩文件路径不存在，需要创建目录
        File dir = zipFile.getParentFile();
        if (!dir.exists()) {
            dir.mkdirs();
        }
        ZipOutputStream out = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(zipFile));
            doCompress(srcFile, out);
        } catch (Exception e) {
            log.error("文件压缩失败:{}", e.getMessage());
            return false;
        } finally {
            // 记得关闭资源
            IOUtils.closeQuietly(out);
        }
        return true;
    }

    private static void doCompress(File file, ZipOutputStream out) throws IOException {
        doCompress(file, out, "");
    }

    /**
     * 递归
     *
     * @param inFile
     * @param out
     * @param dir
     * @throws IOException
     */
    private static void doCompress(File inFile, ZipOutputStream out, String dir) throws IOException {
        if (inFile.isDirectory()) {
            File[] files = inFile.listFiles();
            if (null != files && files.length > 0) {
                for (File file : files) {
                    String name = inFile.getName();
                    if (!"".equals(dir)) {
                        name = dir + File.separator + name;
                    }
                    // 递归
                    ZipUtil.doCompress(file, out, name);
                }
            }
        } else {
            ZipUtil.doZip(inFile, out, dir);
        }
    }

    /**
     * 待压缩文件
     *
     * @param inFile 文件
     * @param out    压缩流
     * @param dir    压缩文件路径
     * @throws IOException
     */
    private static void doZip(File inFile, ZipOutputStream out, String dir) throws IOException {
        FileInputStream fis = null;
        try {
            String entryName = null;
            if (!"".equals(dir)) {
                entryName = dir + File.separator + inFile.getName();
            } else {
                entryName = inFile.getName();
            }
            ZipEntry entry = new ZipEntry(entryName);
            out.putNextEntry(entry);
            int len = 0;
            byte[] buffer = new byte[1024];
            fis = new FileInputStream(inFile);
            while ((len = fis.read(buffer)) > 0) {
                out.write(buffer, 0, len);
                out.flush();
            }
        } finally {
            // 记得关闭资源
            IOUtils.closeQuietly(fis);
        }
    }

}

package com.jiaxw.util.file;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 文件工具类
 *
 * @author jiaxw
 * @date 2020/11/5 14:46
 */
@Slf4j
public class FileUtil {

    /**
     * 文件复制工具类
     *
     * @param file1 复制文件
     * @param file2 粘贴文件
     * @throws IOException IO异常
     */
    public static boolean copy(String file1, String file2) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            File srcFile = new File(file1);
            if (!srcFile.exists()) {
                log.error("要复制的文件不存在");
                throw new IllegalArgumentException("要复制的文件不存在");
            }
            if (!srcFile.isFile()) {
                log.error("复制的不是一个文件");
                throw new IllegalArgumentException("复制的不是一个文件");
            }
            File destFile = new File(file2);
            if (destFile.exists()) {
                if (destFile.isFile()) {
                    log.info("粘贴位置文件名已存在，将被覆盖");
                }
            } else {
                log.info("文件不存在，将创建");
            }

            fis = new FileInputStream(file1);
            fos = new FileOutputStream(file2);
            int b;
            byte[] bytes = new byte[20 * 1024];
            while ((b = fis.read(bytes, 0, bytes.length)) != -1) {
                fos.write(bytes, 0, b);
                fos.flush();
            }

            // 判断是否写入成功
            File destFile2 = new File(file2);
            if (destFile2.exists()) {
                if (destFile2.isFile()) {
                    log.info("[文件复制]:成功");
                    return true;
                }
            }
        } catch (Exception e) {
            log.error("[文件复制]:失败,error:{}", e.getMessage());
            return false;
        } finally {
            if (null != fis) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    log.error("[文件复制]:失败,error:{}", e.getMessage());
                }
            }
        }
        return false;
    }

}

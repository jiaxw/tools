package com.jiaxw.util;

import com.jiaxw.util.file.ZipUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;

/**
 * 压缩包工具类测试类
 *
 * @author jiaxw
 * @date 2020/10/29 15:56
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ToolsApplication.class})
public class ZipUtilTest {

    @Test
    public void doCompressTest() {

        boolean flag = ZipUtil.doCompress("E:/logs", "E:/log/log.zip");
        System.out.println(flag);

        boolean flag2 = ZipUtil.doCompress(new File("E:/logs"), new File("E:/log/log.zip"));
        System.out.println(flag2);

    }

}

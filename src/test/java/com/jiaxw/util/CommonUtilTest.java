package com.jiaxw.util;

import com.jiaxw.util.common.CommonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @RunWith:启动器 SpringJUnit4ClassRunner.class：让 junit 与 spring 环境进行整合
 * @SpringBootTest(classes={ToolsApplication.class}) 1、当前类为 springBoot 的测试类
 * 2、加载 SpringBoot 启动类，启动SpringBoot
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ToolsApplication.class})
public class CommonUtilTest {

    @Test
    public void getUUIDTest() {
        String uuid = CommonUtil.getUUID();
        System.out.println(uuid);
    }

    @Test
    public void getTimeStampTest() {
        String uuid = CommonUtil.getTimeStamp();
        System.out.println(uuid);
    }

    @Test
    public void getRandomTest() {
        String random = CommonUtil.getRandom();
        System.out.println(random);
    }

    @Test
    public void getTimeStampAndRandomTest() {
        String uuid = CommonUtil.getTimeStampAndRandom();
        System.out.println(uuid);
    }

    @Test
    public void getCaptcha() {
        System.out.println(CommonUtil.getCaptcha(6));
        System.out.println(CommonUtil.getCaptcha(CommonUtil.TYPE_DIGITAL, 6));
        System.out.println(CommonUtil.getCaptcha(CommonUtil.TYPE_SMALL, 6));
        System.out.println(CommonUtil.getCaptcha(CommonUtil.TYPE_BIG, 6));
        System.out.println(CommonUtil.getCaptcha(CommonUtil.TYPE_MIXTURE, 6));
    }

}

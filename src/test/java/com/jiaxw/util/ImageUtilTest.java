package com.jiaxw.util;

import com.jiaxw.util.common.CommonUtil;
import com.jiaxw.util.image.ImageUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 压缩图片工具类测试类
 *
 * @author jiaxw
 * @date 2020/10/29 16:50
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ToolsApplication.class})
public class ImageUtilTest {

    @Test
    public void compressTest() throws Exception {
        InputStream in = null;
        try {
            // 原图片的路径
            in = new FileInputStream(new File("E:\\454.jpg"));

            // 缩放后需要保存的路径
            File saveFile = new File("E:\\" + CommonUtil.getTimeStampAndRandom() + ".jpg");

            if (ImageUtil.compress(in, saveFile, 1)) {
                System.out.println("图片压缩成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            in.close();
        }
    }

    @Test
    public void compress2Test() {
        ImageUtil.compress("E:\\454.jpg", "E:\\" + CommonUtil.getTimeStampAndRandom() + ".jpg", 160, 160);
        System.out.println("图片压缩成功！");
    }

    @Test
    public void compress3Test() {
        InputStream in = null;
        try {
            // 原图片的路径
            in = new FileInputStream(new File("E:\\454.jpg"));

            // 缩放后需要保存的路径
            File saveFile = new File("E:\\" + CommonUtil.getTimeStampAndRandom() + ".jpg");

            if (ImageUtil.compress(in, saveFile, 160, 160)) {
                System.out.println("图片压缩成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

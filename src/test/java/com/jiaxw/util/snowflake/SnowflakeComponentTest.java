package com.jiaxw.util.snowflake;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author jiaxw
 * @date 2020/12/30 14:21
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SnowflakeComponentTest {

    @Autowired
    private SnowflakeComponent snowflakeComponent;

    @Test
    public void getOrderNo() {
        for (int i = 0; i < 10; i++) {
            System.out.println(snowflakeComponent.getInstance().nextId());
        }
    }

}

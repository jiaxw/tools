package com.jiaxw.util;

import com.jiaxw.util.image.PicToPdfUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author jiaxw
 * @date 2020/11/30 11:39
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PicToPdfUtilTest {

    @Test
    public void convertTest() {
        String source = "C:\\Users\\pradmin\\Desktop\\未录入\\116.jpg";
        String target = "C:\\Users\\pradmin\\Desktop\\未录入\\116.pdf";
        PicToPdfUtil.convert(source, target);
    }

}

package com.jiaxw.util;

import com.jiaxw.util.regex.PhoneRegExpUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 正则校验手机号 测试类
 *
 * @author jiaxw
 * @date 2020/10/30 21:33
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ToolsApplication.class})
public class PhoneRegExpUtilTest {

    @Test
    public void isChinaPhoneLegalTest() {
        System.out.println(PhoneRegExpUtil.isChinaPhoneLegal("18201989851"));
    }

    @Test
    public void isHkPhoneLegalTest() {
        System.out.println(PhoneRegExpUtil.isHkPhoneLegal("18201989851"));
    }

    @Test
    public void isPhoneLegalTest() {
        System.out.println(PhoneRegExpUtil.isPhoneLegal("18201989851"));
    }

}
package com.jiaxw.util;

import com.jiaxw.util.regex.PhoneLocalUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 手机号本地校验测试类
 *
 * @author jiaxw
 * @date 2020/10/30 21:10
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ToolsApplication.class})
public class PhoneLocalUtilTest {

    @Test
    public void getGeoTest() {
        System.out.println(PhoneLocalUtil.getGeo("18201989851", "86"));
    }

    @Test
    public void getCarrierTest() {
        System.out.println(PhoneLocalUtil.getCarrier("18201989851", "86"));
    }

    @Test
    public void getPhoneRegionCodeTest() {
        System.out.println(PhoneLocalUtil.getPhoneRegionCode("18201989851", "86"));
    }

    @Test
    public void checkPhoneTest() {
        System.out.println(PhoneLocalUtil.checkPhone("18201989851"));
    }

    @Test
    public void checkPhoneNumberTest() {
        System.out.println(PhoneLocalUtil.checkPhoneNumber("18201989851", "86"));
    }

}

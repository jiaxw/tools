package com.jiaxw.util;

import com.jiaxw.util.file.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 文件复制类
 *
 * @author jiaxw
 * @date 2020/11/5 16:22
 */
@Slf4j
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FileUtilTest {

    @Test
    public void fileUtilTest() {
        boolean flag = FileUtil.copy("E:\\SpringBoot-DesignMode-master.zip", "E:\\logs\\SpringBoot-DesignMode-master.zip");
        log.info("[文件复制]:{}", flag);
    }

}

package com.jiaxw.util;

import com.jiaxw.util.image.PdfToTxtUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 * @author jiaxw
 * @date 2020/11/30 16:50
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PdfToTxtUtilTest {

    @Test
    public void getTextFromPdfTest() {
        try {
            // 获取pdf文件路径
            String pdf = PdfToTxtUtil.getTextFromPdf("E:/JAVA核心知识点整理.pdf");
            // 输出到txt文件
            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("E:/JAVA核心知识点整理2.txt"));
            osw.write(pdf);
            osw.flush();
            osw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void readPdfTest() {
        try {
            // 注意此处的绝对地址格式，最好要用这一种。
            PdfToTxtUtil.readPdf("E:/JAVA核心知识点整理.pdf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

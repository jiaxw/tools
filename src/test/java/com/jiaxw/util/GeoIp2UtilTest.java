package com.jiaxw.util;

import com.jiaxw.util.common.GeoIp2Util;
import com.maxmind.geoip2.DatabaseReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * IP定位测试类
 *
 * @author jiaxw
 * @date 2020/10/30 17:43
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ToolsApplication.class})
public class GeoIp2UtilTest {

    @Autowired
    public DatabaseReader databaseReader;

    @Test
    public void getAddressTest() throws Exception {
        String ip = "219.133.170.234";
        String address = GeoIp2Util.getAddress(databaseReader, ip);
        System.out.println("您的IP位置是：" + address);
    }

}
